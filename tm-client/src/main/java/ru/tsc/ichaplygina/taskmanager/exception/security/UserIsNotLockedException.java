package ru.tsc.ichaplygina.taskmanager.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class UserIsNotLockedException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "User is not locked.";

    public UserIsNotLockedException() {
        super(MESSAGE);
    }

}
