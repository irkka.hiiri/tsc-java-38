package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class UserAddCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "add user";

    @NotNull
    public static final String DESCRIPTION = "add a new user";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final String password = readLine(ENTER_PASSWORD);
        @NotNull final String email = readLine("Enter email: ");
        @NotNull final String role = readLine(ENTER_ROLE);
        @NotNull final String firstName = readLine("Enter first name: ");
        @NotNull final String middleName = readLine("Enter middle name: ");
        @NotNull final String lastName = readLine("Enter last name: ");
        getAdminEndpoint().addUser(getSession(), login, password, email, role, firstName, middleName, lastName);
    }

}
