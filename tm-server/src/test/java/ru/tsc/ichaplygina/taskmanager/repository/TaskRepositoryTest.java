package ru.tsc.ichaplygina.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.marker.DatabaseCategory;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.service.ConnectionService;
import ru.tsc.ichaplygina.taskmanager.service.PropertyService;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = NumberUtil.generateId();

    private static final String USER_ID_2 = NumberUtil.generateId();

    private static final String PROJECT_ID_1 = NumberUtil.generateId();

    private static final String PROJECT_ID_2 = NumberUtil.generateId();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private Connection connection;

    @Before
    @SneakyThrows
    public void initRepository() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        taskList = new ArrayList<>();
        connection = connectionService.getConnection();
        taskRepository = new TaskRepository(connection);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("test task " + i);
            task.setDescription("description " + i);
            if (i <= 5) task.setUserId(USER_ID_1);
            else task.setUserId(USER_ID_2);
            task.setProjectId(PROJECT_ID_2);
            try {
                taskRepository.add(task);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            taskList.add(task);
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAddTask() {
        int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final Task newTask = new Task();
        newTask.setUserId(NumberUtil.generateId());
        newTask.setName("test add task");
        try {
            taskRepository.add(newTask);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAdd() {
        int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final String userId = NumberUtil.generateId();
        @NotNull final String name = "test Task";
        @NotNull final String description = "Test Description";
        try {
            taskRepository.add(userId, name, description);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task actualTask = taskRepository.findByName(name);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(userId, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Task> taskList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setUserId(NumberUtil.generateId());
            task.setName("test add all " + i);
            taskList.add(task);
        }
        try {
            taskRepository.addAll(taskList);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAddAllNull() {
        try {
            taskRepository.addAll(null);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAddTaskToProjectPositive() {
        @NotNull final String projectId = NumberUtil.generateId();
        try {
            taskRepository.addTaskToProject(taskList.get(0).getId(), projectId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        @Nullable final Task actualTask = taskRepository.findById(taskList.get(0).getId());
        Assert.assertEquals(projectId, actualTask.getProjectId());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAddTaskToProjectNegative() {
        @NotNull final String projectId = NumberUtil.generateId();
        @NotNull final String taskId = NumberUtil.generateId();
        try {
            taskRepository.addTaskToProject(taskId, projectId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        @Nullable final Task actualTask = taskRepository.findById(taskId);
        Assert.assertNull(actualTask);
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAddTaskToProjectForUserPositive() {
        @NotNull final String projectId = NumberUtil.generateId();
        @NotNull final Task expectedTask = taskList.get(0);
        @NotNull final String userId = expectedTask.getUserId();
        expectedTask.setProjectId(projectId);
        try {
            taskRepository.addTaskToProject(expectedTask.getId(), projectId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        @Nullable final Task actualTask = taskRepository.findByIdForUser(userId, expectedTask.getId());
        Assert.assertEquals(expectedTask.getProjectId(), actualTask.getProjectId());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAddTaskToProjectForUserNegative() {
        @NotNull final String otherUserId = NumberUtil.generateId();
        @NotNull final String projectId = NumberUtil.generateId();
        @NotNull final Task expectedTask = taskList.get(0);
        expectedTask.setProjectId(projectId);
        try {
            taskRepository.addTaskToProject(expectedTask.getId(), projectId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        @Nullable final Task actualTask = taskRepository.findByIdForUser(otherUserId, expectedTask.getId());
        Assert.assertNull(actualTask);
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        try {
            taskRepository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testClearForUserPositive() {
        @NotNull final List<Task> emptyList = new ArrayList<>();
        try {
            taskRepository.clearForUser(USER_ID_1);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(emptyList, taskRepository.findAllForUser(USER_ID_1));
        Assert.assertNotEquals(emptyList, taskRepository.findAllForUser(USER_ID_2));
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testClearForUserNegative() {
        try {
            taskRepository.clearForUser("other_id");
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAll() {
        final int expectedSize = taskRepository.getSize();
        @NotNull final List<Task> actualTaskList = taskRepository.findAll();
        Assert.assertEquals(expectedSize, actualTaskList.size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAllForUser() {
        @NotNull final List<Task> actualTaskList = taskRepository.findAllForUser(USER_ID_1);
        Assert.assertEquals(5, actualTaskList.size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAllByProjectIdPositive() {
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        @NotNull final Task task3 = new Task();
        task1.setProjectId(PROJECT_ID_1);
        task1.setUserId(USER_ID_1);
        task1.setName("test find all by project id 1");
        task2.setProjectId(PROJECT_ID_1);
        task2.setUserId(USER_ID_1);
        task2.setName("test find all by project id 2");
        task3.setProjectId(PROJECT_ID_2);
        task3.setUserId(USER_ID_1);
        task3.setName("test find all by project id 3");
        @NotNull final List<Task> expectedList = new ArrayList<>();
        expectedList.add(task1);
        expectedList.add(task2);
        taskRepository.add(task1);
        taskRepository.add(task2);
        taskRepository.add(task3);
        Assert.assertEquals(expectedList.size(), taskRepository.findAllByProjectId(PROJECT_ID_1).size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAllByProjectIdNegative() {
        @NotNull final List<Task> expectedList = new ArrayList<>();
        Assert.assertEquals(expectedList, taskRepository.findAllByProjectId(PROJECT_ID_1));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAllByProjectIdForUser() {
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        task1.setProjectId(PROJECT_ID_1);
        task1.setUserId(USER_ID_1);
        task1.setName("test find all by project id for user 1");
        task2.setProjectId(PROJECT_ID_1);
        task2.setUserId(USER_ID_2);
        task2.setName("test find all by project id for user 2");
        @NotNull final List<Task> expectedList = new ArrayList<>();
        expectedList.add(task1);
        taskRepository.add(task1);
        taskRepository.add(task2);
        Assert.assertEquals(expectedList.size(), taskRepository.findAllByProjectIdForUser(USER_ID_1, PROJECT_ID_1).size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAllByProjectIdForUserNegative() {
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        task1.setProjectId(PROJECT_ID_1);
        task1.setUserId(USER_ID_1);
        task1.setName("test task for project 1");
        task2.setProjectId(PROJECT_ID_1);
        task2.setUserId(USER_ID_1);
        task2.setName("test task for project 2");
        @NotNull final List<Task> expectedList = new ArrayList<>();
        taskRepository.add(task1);
        taskRepository.add(task2);
        Assert.assertEquals(expectedList, taskRepository.findAllByProjectIdForUser(USER_ID_2, PROJECT_ID_1));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdPositive() {
        for (@NotNull final Task task : taskList)
            Assert.assertNotNull(taskRepository.findById(task.getId()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdNegative() {
        @NotNull final String id = NumberUtil.generateId();
        Assert.assertNull(taskRepository.findById(id));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertNotNull(taskRepository.findByIdForUser(USER_ID_1, task.getId()));
            else Assert.assertNull(taskRepository.findByIdForUser(USER_ID_1, task.getId()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIndex() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(taskRepository.getSize() - 1);
        @NotNull final Task expected3 = taskList.get(taskRepository.getSize() / 2);
        Assert.assertEquals(expected1.getId(), taskRepository.findByIndex(0).getId());
        Assert.assertEquals(expected2.getId(), taskRepository.findByIndex(taskRepository.getSize() - 1).getId());
        Assert.assertEquals(expected3.getId(), taskRepository.findByIndex(taskRepository.getSize() / 2).getId());
        Assert.assertNull(taskRepository.findByIndex(taskRepository.getSize()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIndexForUser() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(5);
        Assert.assertEquals(expected1.getId(), taskRepository.findByIndexForUser(USER_ID_1, 0).getId());
        Assert.assertEquals(expected2.getId(), taskRepository.findByIndexForUser(USER_ID_2, 0).getId());
        Assert.assertNull(taskRepository.findByIndexForUser("unknown user", 0));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByName() {
        for (@NotNull final Task task : taskList) {
            Assert.assertEquals(task.getId(), taskRepository.findByName(task.getName()).getId());
        }
        Assert.assertNull(taskRepository.findByName("???"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByNameForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertEquals(task.getId(), taskRepository.findByNameForUser(USER_ID_1, task.getName()).getId());
            else Assert.assertNull(taskRepository.findByNameForUser(USER_ID_1, task.getName()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindKeysForUser() {
        @NotNull final List<String> expectedList = new ArrayList<>();
        @NotNull final List<String> emptyList = new ArrayList<>();
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) expectedList.add(task.getId());
        }
        Assert.assertEquals(expectedList, taskRepository.findKeysForUser(USER_ID_1));
        Assert.assertEquals(emptyList, taskRepository.findKeysForUser("???"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetId() {
        int count = 0;
        for (@NotNull final Task task : taskList) {
            Assert.assertEquals(task.getId(), taskRepository.getId(task.getName()));
            Assert.assertEquals(task.getId(), taskRepository.getId(count));
            count++;
        }
        Assert.assertNull(taskRepository.getId("Unknown name"));
        Assert.assertNull(taskRepository.getId(taskRepository.getSize()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetIdForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertEquals(task.getId(), taskRepository.getIdForUser(USER_ID_1, task.getName()));
            else Assert.assertNull(task.getId(), taskRepository.getIdForUser(USER_ID_1, task.getName()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSizeForUser() {
        Assert.assertEquals(5, taskRepository.getSizeForUser(USER_ID_1));
        Assert.assertEquals(0, taskRepository.getSizeForUser("Unknown user"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsEmpty() {
        Assert.assertFalse(taskRepository.isEmpty());
        taskRepository.clear();
        Assert.assertTrue(taskRepository.isEmpty());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsEmptyForUser() {
        Assert.assertFalse(taskRepository.isEmptyForUser(USER_ID_1));
        Assert.assertTrue(taskRepository.isEmptyForUser("Unknown user"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsNotFoundById() {
        @NotNull final String validId = taskList.get(0).getId();
        @NotNull final String invalidId = "some id";
        Assert.assertFalse(taskRepository.isNotFoundById(validId));
        Assert.assertTrue(taskRepository.isNotFoundById(invalidId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = taskList.get(0).getId();
        Assert.assertFalse(taskRepository.isNotFoundByIdForUser(USER_ID_1, validId));
        Assert.assertTrue(taskRepository.isNotFoundByIdForUser(USER_ID_2, validId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsNotFoundTaskInProject() {
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertTrue(taskRepository.isNotFoundTaskInProject(taskId, PROJECT_ID_1));
        Assert.assertFalse(taskRepository.isNotFoundTaskInProject(taskId, PROJECT_ID_2));
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveAllByProjectId() {
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        task1.setProjectId(PROJECT_ID_1);
        task1.setUserId(USER_ID_1);
        task1.setName("test remove all by project id 1");
        task2.setProjectId(PROJECT_ID_1);
        task2.setUserId(USER_ID_1);
        task2.setName("test remove all by project id 2");
        @NotNull final List<Task> expectedList = new ArrayList<>();
        expectedList.add(task1);
        expectedList.add(task2);
        try {
            taskRepository.add(task1);
            taskRepository.add(task2);
            taskRepository.removeAllByProjectId(PROJECT_ID_2);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedList.size(), taskRepository.findAll().size());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemovePositive() {
        try {
            for (@NotNull final Task task : taskList) {
                taskRepository.remove(task);
                connection.commit();
                Assert.assertNull(taskRepository.findById(task.getId()));
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveNegative() {
        final int expectedNumberOfEntries = taskRepository.getSize();
        @NotNull final Task taskNotFromRepository = new Task();
        try {
            taskRepository.remove(taskNotFromRepository);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIdPositive() {
        try {
            for (@NotNull final Task task : taskList) {
                @Nullable final Task removedTask = taskRepository.removeById(task.getId());
                connection.commit();
                Assert.assertNotNull(removedTask);
                Assert.assertNull(taskRepository.findById(task.getId()));
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIdNegative() {
        @NotNull final String idNotFromRepository = NumberUtil.generateId();
        try {
            for (@NotNull final Task task : taskList) {
                @Nullable final Task removedTask = taskRepository.removeById(idNotFromRepository);
                connection.commit();
                Assert.assertNull(removedTask);
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIdForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                try {
                    @Nullable final Task userTask = taskRepository.removeByIdForUser(USER_ID_1, task.getId());
                    connection.commit();
                    Assert.assertNotNull(userTask);
                } catch (@NotNull final Exception e) {
                    connection.rollback();
                    throw e;
                }
                Assert.assertNull(taskRepository.findByIdForUser(USER_ID_1, task.getId()));
            } else {
                try {
                    @Nullable final Task userTask = taskRepository.removeByIdForUser(USER_ID_1, task.getId());
                    connection.commit();
                    Assert.assertNull(userTask);
                } catch (@NotNull final Exception e) {
                    connection.rollback();
                    throw e;
                }
            }
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIndex() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(taskRepository.getSize() - 1);
        @NotNull final Task expected3 = taskList.get(taskRepository.getSize() / 2);
        try {
            @Nullable final Task task1 = taskRepository.removeByIndex(0);
            @Nullable final Task task2 = taskRepository.removeByIndex(taskRepository.getSize() - 1);
            @Nullable final Task task3 = taskRepository.removeByIndex(taskRepository.getSize() / 2);
            taskRepository.removeByIndex(taskRepository.getSize());
            connection.commit();
            Assert.assertEquals(expected1.getId(), task1.getId());
            Assert.assertEquals(expected2.getId(), task2.getId());
            Assert.assertEquals(expected3.getId(), task3.getId());
            Assert.assertEquals(NUMBER_OF_ENTRIES - 3, taskRepository.getSize());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIndexForUser() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(5);
        try {
            @Nullable final Task task1 = taskRepository.removeByIndexForUser(USER_ID_1, 0);
            @Nullable final Task task2 = taskRepository.removeByIndexForUser(USER_ID_2, 0);
            @Nullable final Task task3 = taskRepository.removeByIndexForUser("unknown user", 0);
            connection.commit();
            Assert.assertEquals(expected1.getId(), task1.getId());
            Assert.assertEquals(expected2.getId(), task2.getId());
            Assert.assertNull(task3);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByName() {
        try {
            for (@NotNull final Task task : taskList) {
                @Nullable final Task removedTask = taskRepository.removeByName(task.getName());
                connection.commit();
                Assert.assertEquals(task.getId(), removedTask.getId());
            }
            @Nullable final Task removedTask = taskRepository.removeByName("???");
            connection.commit();
            Assert.assertNull(removedTask);
            Assert.assertEquals(0, taskRepository.getSize());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByNameForUser() {
        try {
            for (@NotNull final Task task : taskList) {
                if (task.getUserId().equals(USER_ID_1)) {
                    @Nullable final Task removedTask = taskRepository.removeByNameForUser(USER_ID_1, task.getName());
                    connection.commit();
                    Assert.assertEquals(task.getId(), removedTask.getId());
                } else {
                    @Nullable final Task removedTask = taskRepository.removeByNameForUser(USER_ID_1, task.getName());
                    connection.commit();
                    Assert.assertNull(removedTask);
                }
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(NUMBER_OF_ENTRIES - 5, taskRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveTaskFromProject() {
        try {
            @NotNull final Task task = taskList.get(0);
            @Nullable final Task task1 = taskRepository.removeTaskFromProject(task.getId(), PROJECT_ID_2);
            connection.commit();
            @Nullable final Task task2 = taskRepository.removeTaskFromProject(task.getId(), PROJECT_ID_1);
            connection.commit();
            Assert.assertEquals(task.getId(), task1.getId());
            Assert.assertNull(task2);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(0, taskRepository.findAllByProjectId(PROJECT_ID_1).size());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveTaskFromProjectForUser() {
        try {
            for (@NotNull final Task task : taskList) {
                if (task.getUserId().equals(USER_ID_1)) {
                    @Nullable final Task removedTask = taskRepository.removeTaskFromProjectForUser(USER_ID_1, task.getId(), PROJECT_ID_2);
                    connection.commit();
                    Assert.assertEquals(task.getId(), removedTask.getId());
                } else {
                    @Nullable final Task removedTask = taskRepository.removeTaskFromProjectForUser(USER_ID_1, task.getId(), PROJECT_ID_2);
                    connection.commit();
                    Assert.assertNull(removedTask);
                }
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(NUMBER_OF_ENTRIES - 5, taskRepository.findAllByProjectId(PROJECT_ID_2).size());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdatePositive() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskId = taskExpected.getId();
        @NotNull final String newName = "test new name";
        @NotNull final String newDescription = "new description";
        taskExpected.setName(newName);
        taskExpected.setDescription(newDescription);
        try {
            @Nullable final Task taskActual = taskRepository.update(taskId, newName, newDescription);
            connection.commit();
            Assert.assertEquals(taskExpected.getId(), taskActual.getId());
            Assert.assertEquals(taskExpected.getName(), taskActual.getName());
            Assert.assertEquals(taskExpected.getDescription(), taskActual.getDescription());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateNegative() {
        @NotNull final String taskId = NumberUtil.generateId();
        @NotNull final String newName = "test name";
        @NotNull final String newDescription = "new description";
        try {
            @Nullable final Task taskActual = taskRepository.update(taskId, newName, newDescription);
            connection.commit();
            Assert.assertNull(taskActual);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateForUserPositive() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskId = taskExpected.getId();
        @NotNull final String newName = "testname";
        @NotNull final String newDescription = "new description";
        taskExpected.setName(newName);
        taskExpected.setDescription(newDescription);
        try {
            @Nullable final Task taskActual = taskRepository.updateForUser(USER_ID_1, taskId, newName, newDescription);
            connection.commit();
            Assert.assertEquals(taskExpected.getId(), taskActual.getId());
            Assert.assertEquals(taskExpected.getName(), taskActual.getName());
            Assert.assertEquals(taskExpected.getDescription(), taskActual.getDescription());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateForUserNegative() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskId = taskExpected.getId();
        @NotNull final String newName = "test test new name";
        @NotNull final String newDescription = "new description";
        taskExpected.setName(newName);
        taskExpected.setDescription(newDescription);
        try {
            @Nullable final Task taskActual = taskRepository.updateForUser(USER_ID_2, taskId, newName, newDescription);
            connection.commit();
            Assert.assertNull(taskActual);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateStatusById() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskId = taskExpected.getId();
        taskExpected.setStatus(Status.COMPLETED);
        try {
            @Nullable final Task taskActual = taskRepository.updateStatusById(taskId, Status.COMPLETED);
            connection.commit();
            Assert.assertNotNull(taskActual);
            Assert.assertEquals(taskExpected.getStatus(), taskActual.getStatus());
            Assert.assertNotNull(taskActual.getDateFinish());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateStatusByIdForUser() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskId = taskExpected.getId();
        taskExpected.setStatus(Status.IN_PROGRESS);
        try {
            @Nullable final Task taskActual = taskRepository.updateStatusByIdForUser(USER_ID_1, taskId, Status.IN_PROGRESS);
            connection.commit();
            Assert.assertNotNull(taskActual);
            Assert.assertEquals(taskExpected.getStatus(), taskActual.getStatus());
            Assert.assertNotNull(taskActual.getDateStart());
            Assert.assertNull(taskRepository.updateStatusByIdForUser(USER_ID_2, taskId, Status.IN_PROGRESS));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateStatusByIndex() {
        @NotNull final Task taskExpected = taskList.get(0);
        taskExpected.setStatus(Status.COMPLETED);
        try {
            @Nullable final Task taskActual = taskRepository.updateStatusByIndex(0, Status.COMPLETED);
            connection.commit();
            Assert.assertNotNull(taskActual);
            Assert.assertEquals(taskExpected.getStatus(), taskActual.getStatus());
            Assert.assertNotNull(taskActual.getDateFinish());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateStatusByIndexForUser() {
        @NotNull final Task taskExpected = taskList.get(0);
        taskExpected.setStatus(Status.IN_PROGRESS);
        try {
            @Nullable final Task taskActual = taskRepository.updateStatusByIndexForUser(USER_ID_1, 0, Status.IN_PROGRESS);
            connection.commit();
            Assert.assertNotNull(taskActual);
            Assert.assertEquals(taskExpected.getStatus(), taskActual.getStatus());
            Assert.assertNotNull(taskActual.getDateStart());
            Assert.assertNull(taskRepository.updateStatusByIndexForUser("unknown user", 0, Status.IN_PROGRESS));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateStatusByName() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskName = taskExpected.getName();
        taskExpected.setStatus(Status.COMPLETED);
        try {
            @Nullable final Task taskActual = taskRepository.updateStatusByName(taskName, Status.COMPLETED);
            connection.commit();
            Assert.assertNotNull(taskActual);
            Assert.assertEquals(taskExpected.getStatus(), taskActual.getStatus());
            Assert.assertNotNull(taskActual.getDateFinish());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateStatusByNameForUser() {
        @NotNull final Task taskExpected = taskList.get(0);
        @NotNull final String taskName = taskExpected.getName();
        taskExpected.setStatus(Status.IN_PROGRESS);
        try {
            @Nullable final Task taskActual = taskRepository.updateStatusByNameForUser(USER_ID_1, taskName, Status.IN_PROGRESS);
            connection.commit();
            Assert.assertNotNull(taskActual);
            Assert.assertEquals(taskExpected.getStatus(), taskActual.getStatus());
            Assert.assertNotNull(taskActual.getDateStart());
            Assert.assertNull(taskRepository.updateStatusByIdForUser(USER_ID_2, taskName, Status.IN_PROGRESS));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @After
    @SneakyThrows
    public void closeConnection() {
        try {
            @NotNull final String sql = "DELETE FROM TM_TASK WHERE NAME LIKE 'test%'";
            @NotNull final Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            statement.close();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
        } finally {
            connection.close();
        }
    }

}
