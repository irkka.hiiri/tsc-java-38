package ru.tsc.ichaplygina.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.api.repository.ISessionRepository;
import ru.tsc.ichaplygina.taskmanager.marker.DatabaseCategory;
import ru.tsc.ichaplygina.taskmanager.model.Session;
import ru.tsc.ichaplygina.taskmanager.service.ConnectionService;
import ru.tsc.ichaplygina.taskmanager.service.PropertyService;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionRepository sessionRepository;

    @NotNull
    private Connection connection;

    @Before
    @SneakyThrows
    public void initRepository() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionList = new ArrayList<>();
        connection = connectionService.getConnection();
        sessionRepository = new SessionRepository(connection);
        try {
            for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
                @NotNull final Session session = new Session("test" + NumberUtil.generateId());
                sessionRepository.add(session);
                connection.commit();
                sessionList.add(session);
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAdd() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final Session newSession = new Session();
        newSession.setId(NumberUtil.generateId());
        newSession.setUserId("test" + NumberUtil.generateId());
        try {
            sessionRepository.add(newSession);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAddAll() {
        final int expectedNumberOfEntries = sessionRepository.getSize() + NUMBER_OF_ENTRIES;
        @NotNull List<Session> sessionList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session newSession = new Session();
            newSession.setId(NumberUtil.generateId());
            newSession.setUserId("test" + NumberUtil.generateId());
            sessionList.add(newSession);
        }
        try {
            sessionRepository.addAll(sessionList);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAddAllNull() {
        final int expectedSize = sessionRepository.getSize();
        try {
            sessionRepository.addAll(null);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedSize, sessionRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        try {
            sessionRepository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAll() {
        @NotNull final List<Session> actualSessionList = sessionRepository.findAll();
        Assert.assertEquals(sessionList.size(), actualSessionList.size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdPositive() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(sessionRepository.findById(session.getId()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdNegative() {
        @NotNull final String id = NumberUtil.generateId();
        Assert.assertNull(sessionRepository.findById(id));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsEmpty() {
        Assert.assertFalse(sessionRepository.isEmpty());
        sessionRepository.clear();
        Assert.assertTrue(sessionRepository.isEmpty());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemovePositive() {
        try {
            for (@NotNull final Session session : sessionList) {
                sessionRepository.remove(session);
                connection.commit();
                Assert.assertNull(sessionRepository.findById(session.getId()));
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveNegative() {
        final int expectedSize = sessionRepository.getSize();
        @NotNull final Session sessionNotFromRepository = new Session();
        try {
            sessionRepository.remove(sessionNotFromRepository);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }

        Assert.assertEquals(expectedSize, sessionRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIdPositive() {
        try {
            for (@NotNull final Session session : sessionList) {
                @Nullable final Session removedSession = sessionRepository.removeById(session.getId());
                Assert.assertNotNull(removedSession);
                connection.commit();
                Assert.assertNull(sessionRepository.findById(session.getId()));
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIdNegative() {
        @NotNull final String idNotFromRepository = NumberUtil.generateId();
        try {
            @Nullable final Session removedSession = sessionRepository.removeById(idNotFromRepository);
            connection.commit();
            Assert.assertNull(removedSession);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @After
    @SneakyThrows
    public void closeConnection() {
        try {
            @NotNull final String sql = "DELETE FROM TM_SESSION WHERE USER_ID LIKE 'test%'";
            @NotNull final Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            statement.close();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
        } finally {
            connection.close();
        }
    }

}
