package ru.tsc.ichaplygina.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.marker.DatabaseCategory;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.service.ConnectionService;
import ru.tsc.ichaplygina.taskmanager.service.PropertyService;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = NumberUtil.generateId();

    private static final String USER_ID_2 = NumberUtil.generateId();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private Connection connection;

    @Before
    @SneakyThrows
    public void initRepository() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        projectList = new ArrayList<>();
        connection = connectionService.getConnection();
        projectRepository = new ProjectRepository(connection);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("test project " + i);
            project.setDescription("description " + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            try {
                projectRepository.add(project);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            projectList.add(project);
        }
    }

    @Test
    @SneakyThrows
    @Category(DatabaseCategory.class)
    public void testAddProject() {
        int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final Project newProject = new Project();
        newProject.setId(NumberUtil.generateId());
        newProject.setName("test test test tes te s");
        newProject.setUserId(NumberUtil.generateId());
        try {
            projectRepository.add(newProject);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    @SneakyThrows
    @Category(DatabaseCategory.class)
    public void testAdd() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = NumberUtil.generateId();
        @NotNull final String name = "test Project";
        @NotNull final String description = "Test Description";
        try {
            projectRepository.add(userId, name, description);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        @Nullable final Project actualProject = projectRepository.findByIndex(projectRepository.getSize() - 1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(userId, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(DatabaseCategory.class)
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setId(NumberUtil.generateId());
            project.setName("test tes test" + i);
            project.setUserId(NumberUtil.generateId());
            projectList.add(project);
        }
        try {
            projectRepository.addAll(projectList);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    @SneakyThrows
    @Category(DatabaseCategory.class)
    public void testAddAllNull() {
        try {
            projectRepository.addAll(null);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    @SneakyThrows
    @Category(DatabaseCategory.class)
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        try {
            projectRepository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    @SneakyThrows
    @Category(DatabaseCategory.class)
    public void testClearForUserPositive() {
        @NotNull final List<Project> emptyList = new ArrayList<>();
        try {
            projectRepository.clearForUser(USER_ID_1);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(emptyList, projectRepository.findAllForUser(USER_ID_1));
        Assert.assertNotEquals(emptyList, projectRepository.findAllForUser(USER_ID_2));
    }

    @Test
    @SneakyThrows
    @Category(DatabaseCategory.class)
    public void testClearForUserNegative() {
        try {
            projectRepository.clearForUser("other_id");
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAll() {
        @NotNull final List<Project> actualProjectList = projectRepository.findAll();
        Assert.assertEquals(projectRepository.getSize(), actualProjectList.size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAllForUser() {
        @NotNull final List<Project> actualProjectList = new ArrayList<>(projectRepository.findAllForUser(USER_ID_1));
        Assert.assertEquals(5, actualProjectList.size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdPositive() {
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectRepository.findById(project.getId()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdNegative() {
        @NotNull final String id = NumberUtil.generateId();
        Assert.assertNull(projectRepository.findById(id));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdForUser() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(projectRepository.findByIdForUser(USER_ID_1, project.getId()));
            } else Assert.assertNull(projectRepository.findByIdForUser(USER_ID_1, project.getId()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIndex() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(projectRepository.getSize() - 1);
        @NotNull final Project expected3 = projectList.get(projectRepository.getSize() / 2);
        Assert.assertEquals(expected1.getId(), projectRepository.findByIndex(0).getId());
        Assert.assertEquals(expected2.getId(), projectRepository.findByIndex(projectRepository.getSize() - 1).getId());
        Assert.assertEquals(expected3.getId(), projectRepository.findByIndex(projectRepository.getSize() / 2).getId());
        Assert.assertNull(projectRepository.findByIndex(projectRepository.getSize()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIndexForUser() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(5);
        Assert.assertEquals(expected1.getId(), projectRepository.findByIndexForUser(USER_ID_1, 0).getId());
        Assert.assertEquals(expected2.getId(), projectRepository.findByIndexForUser(USER_ID_2, 0).getId());
        Assert.assertNull(projectRepository.findByIndexForUser("unknown user", 0));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByName() {
        for (@NotNull final Project project : projectList) {
            Assert.assertEquals(project.getId(), projectRepository.findByName(project.getName()).getId());
        }
        Assert.assertNull(projectRepository.findByName("???"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByNameForUser() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                Assert.assertEquals(project.getId(), projectRepository.findByNameForUser(USER_ID_1, project.getName()).getId());
            else Assert.assertNull(projectRepository.findByNameForUser(USER_ID_1, project.getName()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindKeysForUser() {
        @NotNull final List<String> expectedList = new ArrayList<>();
        @NotNull final List<String> emptyList = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) expectedList.add(project.getId());
        }
        Assert.assertEquals(expectedList, projectRepository.findKeysForUser(USER_ID_1));
        Assert.assertEquals(emptyList, projectRepository.findKeysForUser("???"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetId() {
        int count = 0;
        for (@NotNull final Project project : projectList) {
            Assert.assertEquals(project.getId(), projectRepository.getId(project.getName()));
            Assert.assertEquals(project.getId(), projectRepository.getId(count));
            count++;
        }
        Assert.assertNull(projectRepository.getId("Unknown name"));
        Assert.assertNull(projectRepository.getId(projectRepository.getSize()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetIdForUser() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                Assert.assertEquals(project.getId(), projectRepository.getIdForUser(USER_ID_1, project.getName()));
            else Assert.assertNull(project.getId(), projectRepository.getIdForUser(USER_ID_1, project.getName()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSizeForUser() {
        Assert.assertEquals(5, projectRepository.getSizeForUser(USER_ID_1));
        Assert.assertEquals(0, projectRepository.getSizeForUser("Unknown user"));
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testIsEmpty() {
        Assert.assertFalse(projectRepository.isEmpty());
        try {
            projectRepository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertTrue(projectRepository.isEmpty());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsEmptyForUser() {
        Assert.assertFalse(projectRepository.isEmptyForUser(USER_ID_1));
        Assert.assertTrue(projectRepository.isEmptyForUser("Unknown user"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsNotFoundById() {
        @NotNull final String validId = projectList.get(0).getId();
        @NotNull final String invalidId = "some id";
        Assert.assertFalse(projectRepository.isNotFoundById(validId));
        Assert.assertTrue(projectRepository.isNotFoundById(invalidId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = projectList.get(0).getId();
        Assert.assertFalse(projectRepository.isNotFoundByIdForUser(USER_ID_1, validId));
        Assert.assertTrue(projectRepository.isNotFoundByIdForUser(USER_ID_2, validId));
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemovePositive() {
        try {
            for (@NotNull final Project project : projectList) {
                projectRepository.remove(project);
                connection.commit();
                Assert.assertNull(projectRepository.findById(project.getId()));
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveNegative() {
        final int expectedSize = projectRepository.getSize();
        @NotNull final Project projectNotFromRepository = new Project();
        try {
            projectRepository.remove(projectNotFromRepository);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedSize, projectRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIdPositive() {
        try {
            for (@NotNull final Project project : projectList) {
                @Nullable final Project removedProject = projectRepository.removeById(project.getId());
                connection.commit();
                Assert.assertNotNull(removedProject);
                Assert.assertNull(projectRepository.findById(project.getId()));
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIdNegative() {
        @NotNull final String idNotFromRepository = NumberUtil.generateId();
        try {
            for (@NotNull final Project project : projectList) {
                @Nullable final Project removedProject = projectRepository.removeById(idNotFromRepository);
                connection.commit();
                Assert.assertNull(removedProject);
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIdForUser() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                try {
                    @Nullable final Project userProject = projectRepository.removeByIdForUser(USER_ID_1, project.getId());
                    connection.commit();
                    Assert.assertNotNull(userProject);
                } catch (@NotNull final Exception e) {
                    connection.rollback();
                    throw e;
                }
                Assert.assertNull(projectRepository.findByIdForUser(USER_ID_1, project.getId()));
            } else {
                try {
                    @Nullable final Project userProject = projectRepository.removeByIdForUser(USER_ID_1, project.getId());
                    connection.commit();
                    Assert.assertNull(userProject);
                } catch (@NotNull final Exception e) {
                    connection.rollback();
                    throw e;
                }
            }
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIndex() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(projectRepository.getSize() - 1);
        @NotNull final Project expected3 = projectList.get(projectRepository.getSize() / 2);
        try {
            @Nullable final Project project1 = projectRepository.removeByIndex(0);
            @Nullable final Project project2 = projectRepository.removeByIndex(projectRepository.getSize() - 1);
            @Nullable final Project project3 = projectRepository.removeByIndex(projectRepository.getSize() / 2);
            projectRepository.removeByIndex(projectRepository.getSize());
            connection.commit();
            Assert.assertEquals(expected1.getId(), project1.getId());
            Assert.assertEquals(expected2.getId(), project2.getId());
            Assert.assertEquals(expected3.getId(), project3.getId());
            Assert.assertEquals(NUMBER_OF_ENTRIES - 3, projectRepository.getSize());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIndexForUser() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(5);
        try {
            @Nullable final Project project1 = projectRepository.removeByIndexForUser(USER_ID_1, 0);
            @Nullable final Project project2 = projectRepository.removeByIndexForUser(USER_ID_2, 0);
            @Nullable final Project project3 = projectRepository.removeByIndexForUser("unknown user", 0);
            connection.commit();
            Assert.assertEquals(expected1.getId(), project1.getId());
            Assert.assertEquals(expected2.getId(), project2.getId());
            Assert.assertNull(project3);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByName() {
        try {
            for (@NotNull final Project project : projectList) {
                @Nullable final Project removedProject = projectRepository.removeByName(project.getName());
                connection.commit();
                Assert.assertEquals(project.getId(), removedProject.getId());
            }
            @Nullable final Project removedProject = projectRepository.removeByName("???");
            connection.commit();
            Assert.assertNull(removedProject);
            Assert.assertEquals(0, projectRepository.getSize());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByNameForUser() {
        try {
            for (@NotNull final Project project : projectList) {
                if (project.getUserId().equals(USER_ID_1)) {
                    @Nullable final Project removedProject = projectRepository.removeByNameForUser(USER_ID_1, project.getName());
                    connection.commit();
                    Assert.assertEquals(project.getId(), removedProject.getId());
                } else {
                    @Nullable final Project removedProject = projectRepository.removeByNameForUser(USER_ID_1, project.getName());
                    connection.commit();
                    Assert.assertNull(removedProject);
                }
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(NUMBER_OF_ENTRIES - 5, projectRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdatePositive() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectId = projectExpected.getId();
        @NotNull final String newName = "test project 777";
        @NotNull final String newDescription = "new description";
        projectExpected.setName(newName);
        projectExpected.setDescription(newDescription);
        try {
            @Nullable final Project projectActual = projectRepository.update(projectId, newName, newDescription);
            connection.commit();
            Assert.assertEquals(projectExpected.getId(), projectActual.getId());
            Assert.assertEquals("test project 777", projectActual.getName());
            Assert.assertEquals("new description", projectActual.getDescription());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateNegative() {
        @NotNull final String projectId = NumberUtil.generateId();
        @NotNull final String newName = "test project 888";
        @NotNull final String newDescription = "new description";
        try {
            @Nullable final Project projectActual = projectRepository.update(projectId, newName, newDescription);
            connection.commit();
            Assert.assertNull(projectActual);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateForUserPositive() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectId = projectExpected.getId();
        @NotNull final String newName = "test project 999";
        @NotNull final String newDescription = "new description";
        projectExpected.setName(newName);
        projectExpected.setDescription(newDescription);
        try {
            @Nullable final Project projectActual = projectRepository.updateForUser(USER_ID_1, projectId, newName, newDescription);
            connection.commit();
            Assert.assertEquals(projectExpected.getId(), projectActual.getId());
            Assert.assertEquals(projectExpected.getName(), projectActual.getName());
            Assert.assertEquals(projectExpected.getDescription(), projectActual.getDescription());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }

    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateForUserNegative() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectId = projectExpected.getId();
        @NotNull final String newName = "test project 100500";
        @NotNull final String newDescription = "new description";
        projectExpected.setName(newName);
        projectExpected.setDescription(newDescription);
        try {
            @Nullable final Project projectActual = projectRepository.updateForUser(USER_ID_2, projectId, newName, newDescription);
            connection.commit();
            Assert.assertNull(projectActual);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateStatusById() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectId = projectExpected.getId();
        projectExpected.setStatus(Status.COMPLETED);
        try {
            @Nullable final Project projectActual = projectRepository.updateStatusById(projectId, Status.COMPLETED);
            connection.commit();
            Assert.assertNotNull(projectActual);
            Assert.assertEquals(projectExpected.getStatus(), projectActual.getStatus());
            Assert.assertNotNull(projectActual.getDateFinish());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateStatusByIdForUser() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectId = projectExpected.getId();
        projectExpected.setStatus(Status.IN_PROGRESS);
        try {
            @Nullable final Project projectActual = projectRepository.updateStatusByIdForUser(USER_ID_1, projectId, Status.IN_PROGRESS);
            connection.commit();
            Assert.assertNotNull(projectActual);
            Assert.assertEquals(projectExpected.getStatus(), projectActual.getStatus());
            Assert.assertNotNull(projectActual.getDateStart());
            Assert.assertNull(projectRepository.updateStatusByIdForUser(USER_ID_2, projectId, Status.IN_PROGRESS));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateStatusByIndex() {
        @NotNull final Project projectExpected = projectList.get(0);
        projectExpected.setStatus(Status.COMPLETED);
        try {
            @Nullable final Project projectActual = projectRepository.updateStatusByIndex(0, Status.COMPLETED);
            connection.commit();
            Assert.assertNotNull(projectActual);
            Assert.assertEquals(projectExpected.getStatus(), projectActual.getStatus());
            Assert.assertNotNull(projectActual.getDateFinish());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateStatusByIndexForUser() {
        @NotNull final Project projectExpected = projectList.get(0);
        projectExpected.setStatus(Status.IN_PROGRESS);
        try {
            @Nullable final Project projectActual = projectRepository.updateStatusByIndexForUser(USER_ID_1, 0, Status.IN_PROGRESS);
            connection.commit();
            Assert.assertNotNull(projectActual);
            Assert.assertEquals(projectExpected.getStatus(), projectActual.getStatus());
            Assert.assertNotNull(projectActual.getDateStart());
            Assert.assertNull(projectRepository.updateStatusByIndexForUser("unknown user", 0, Status.IN_PROGRESS));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateStatusByName() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectName = projectExpected.getName();
        projectExpected.setStatus(Status.COMPLETED);
        try {
            @Nullable final Project projectActual = projectRepository.updateStatusByName(projectName, Status.COMPLETED);
            connection.commit();
            Assert.assertNotNull(projectActual);
            Assert.assertEquals(projectExpected.getStatus(), projectActual.getStatus());
            Assert.assertNotNull(projectActual.getDateFinish());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateStatusByNameForUser() {
        @NotNull final Project projectExpected = projectList.get(0);
        @NotNull final String projectName = projectExpected.getName();
        projectExpected.setStatus(Status.IN_PROGRESS);
        try {
            @Nullable final Project projectActual = projectRepository.updateStatusByNameForUser(USER_ID_1, projectName, Status.IN_PROGRESS);
            connection.commit();
            Assert.assertNotNull(projectActual);
            Assert.assertEquals(projectExpected.getStatus(), projectActual.getStatus());
            Assert.assertNotNull(projectActual.getDateStart());
            Assert.assertNull(projectRepository.updateStatusByIdForUser(USER_ID_2, projectName, Status.IN_PROGRESS));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @After
    @SneakyThrows
    public void closeConnection() {
        try {
            @NotNull final String sql = "DELETE FROM TM_PROJECT WHERE NAME LIKE 'test%'";
            @NotNull final Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            statement.close();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
        } finally {
            connection.close();
        }
    }

}
