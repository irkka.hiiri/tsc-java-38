package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.ISessionService;
import ru.tsc.ichaplygina.taskmanager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "SessionEndpoint")
public class SessionEndpoint {

    @NotNull
    private final ISessionService sessionService;

    public SessionEndpoint(@NotNull ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    @WebMethod
    public Session openSession(@WebParam(name = "login") @NotNull final String login,
                               @WebParam(name = "password") @NotNull final String password) {
        return sessionService.openSession(login, password);
    }

    @WebMethod
    public void closeSession(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.closeSession(session);
    }

}
