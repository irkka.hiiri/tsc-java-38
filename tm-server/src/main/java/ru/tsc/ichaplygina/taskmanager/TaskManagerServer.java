package ru.tsc.ichaplygina.taskmanager;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.bootstrap.Bootstrap;

public final class TaskManagerServer {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
