package ru.tsc.ichaplygina.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;

public final class UserRepository extends AbstractModelRepository<User> implements IUserRepository {

    @NotNull
    private static final String TABLE_NAME = "TM_USER";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final User user) {
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                "(ID, LOGIN, PASSWORD, EMAIL, ROLE, FIRST_NAME, MIDDLE_NAME, LAST_NAME, LOCKED) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getEmail());
        statement.setString(5, user.getRole().toString());
        statement.setString(6, user.getFirstName());
        statement.setString(7, user.getMiddleName());
        statement.setString(8, user.getLastName());
        statement.setBoolean(9, user.isLocked());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public @NotNull User fetch(@NotNull ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("ID"));
        user.setLogin(row.getString("LOGIN"));
        user.setPasswordHash(row.getString("PASSWORD"));
        user.setEmail(row.getString("EMAIL"));
        user.setRole(Role.getRole(row.getString("ROLE")));
        user.setFirstName(row.getString("FIRST_NAME"));
        user.setMiddleName(row.getString("MIDDLE_NAME"));
        user.setLastName(row.getString("LAST_NAME"));
        user.setLocked(row.getBoolean("LOCKED"));
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public final User findByEmail(@NotNull final String email) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE EMAIL = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, email);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final User user = fetch(resultSet);
        statement.close();
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public final User findByLogin(@NotNull final String login) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE LOGIN = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final User user = fetch(resultSet);
        statement.close();
        return user;
    }

    @Nullable
    @Override
    public final String findIdByLogin(@NotNull final String login) {
        final Optional<User> user = Optional.ofNullable(findByLogin(login));
        return user.map(User::getId).orElse(null);
    }

    @Override
    public final boolean isFoundByEmail(@NotNull final String email) {
        return (findByEmail(email)) != null;
    }

    @Override
    public final boolean isFoundByLogin(@NotNull final String login) {
        return (findByLogin(login)) != null;
    }

    @Override
    public final void removeByLogin(@NotNull final String login) {
        Optional.ofNullable(findByLogin(login)).ifPresent(this::remove);
    }

    @Nullable
    @Override
    @SneakyThrows
    public final User update(@NotNull final String id, @NotNull final String login, @NotNull final String password, @NotNull final String email,
                             @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        @NotNull final String sql = "UPDATE " + getTableName() + " SET LOGIN = ?, PASSWORD = ?, EMAIL = ?, ROLE = ?, " +
                "FIRST_NAME = ?, MIDDLE_NAME = ?, LAST_NAME = ? WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, login);
        statement.setString(2, password);
        statement.setString(3, email);
        statement.setString(4, role.toString());
        statement.setString(5, firstName);
        statement.setString(6, middleName);
        statement.setString(7, lastName);
        statement.setString(8, id);
        statement.executeUpdate();
        statement.close();
        return findById(id);
    }

    @Override
    @SneakyThrows
    public final int setLocked(@NotNull final String id, final boolean locked) {
        @NotNull final String sql = "UPDATE " + getTableName() + " SET LOCKED = ? WHERE ID = ? AND LOCKED != ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setBoolean(1, locked);
        statement.setString(2, id);
        statement.setBoolean(3, locked);
        final int rowsUpdated = statement.executeUpdate();
        statement.close();
        return rowsUpdated;
    }

    @Override
    @SneakyThrows
    public final void setPassword(@NotNull final String id, @NotNull final String password) {
        @NotNull final String sql = "UPDATE " + getTableName() + " SET PASSWORD = ? WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, password);
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public final void setRole(@NotNull final String id, @NotNull final Role role) {
        @NotNull final String sql = "UPDATE " + getTableName() + " SET ROLE = ? WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, role.toString());
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

}
