package ru.tsc.ichaplygina.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractBusinessEntityRepository<Project> implements IProjectRepository {

    @NotNull
    private static final String TABLE_NAME = "TM_PROJECT";

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    @SneakyThrows
    public @NotNull Project fetch(@NotNull ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString("ID"));
        project.setName(row.getString("NAME"));
        project.setDescription(row.getString("DESCRIPTION"));
        project.setUserId(row.getString("USER_ID"));
        project.setStatus(Status.getStatus(row.getString("STATUS")));
        project.setCreated(row.getTimestamp("CREATED_DT"));
        project.setDateStart(row.getTimestamp("STARTED_DT"));
        project.setDateFinish(row.getTimestamp("COMPLETED_DT"));
        return project;
    }

    @Override
    @SneakyThrows
    public final void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        @NotNull final Project project = new Project(name, description, userId);
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                "(ID, NAME, DESCRIPTION, STATUS, CREATED_DT, USER_ID) VALUES (?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, project.getId());
        statement.setString(2, name);
        statement.setString(3, description);
        statement.setString(4, project.getStatus().toString());
        statement.setTimestamp(5, new Timestamp(project.getCreated().getTime()));
        statement.setString(6, userId);
        statement.executeUpdate();
        statement.close();
    }

}
