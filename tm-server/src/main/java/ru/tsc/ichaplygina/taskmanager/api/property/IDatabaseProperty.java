package ru.tsc.ichaplygina.taskmanager.api.property;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull String getDatabasePassword();

    @NotNull String getDatabaseUrl();

    @NotNull String getDatabaseUsername();

}
