package ru.tsc.ichaplygina.taskmanager.api.property;

import org.jetbrains.annotations.NotNull;

public interface INetworkProperty {
    @NotNull String getServer();

    @NotNull String getPort();
}
