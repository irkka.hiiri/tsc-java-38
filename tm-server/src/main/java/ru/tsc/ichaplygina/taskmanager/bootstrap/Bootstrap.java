package ru.tsc.ichaplygina.taskmanager.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.ServiceLocator;
import ru.tsc.ichaplygina.taskmanager.api.service.*;
import ru.tsc.ichaplygina.taskmanager.component.Backup;
import ru.tsc.ichaplygina.taskmanager.endpoint.AdminEndpoint;
import ru.tsc.ichaplygina.taskmanager.endpoint.ProjectEndpoint;
import ru.tsc.ichaplygina.taskmanager.endpoint.SessionEndpoint;
import ru.tsc.ichaplygina.taskmanager.endpoint.TaskEndpoint;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.service.*;
import ru.tsc.ichaplygina.taskmanager.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
public final class Bootstrap implements ServiceLocator {

    private static final boolean CONSOLE_LOG_ENABLED = false;

    @NotNull
    private static final String PID_FILE_NAME = "task-manager.pid";

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ILogService logService = new LogService(CONSOLE_LOG_ENABLED);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService, userService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService, userService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskService, projectService);

    @NotNull
    private final IDomainService domainService = new DomainService(projectService, taskService, userService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, propertyService, userService);

    @NotNull
    private final Backup backup = new Backup(domainService, propertyService);

    @SneakyThrows
    public void initEndpoints() {
        @NotNull final String url = propertyService.getServer() + ":" + propertyService.getPort();
        Endpoint.publish(url + "/SessionEndpoint", new SessionEndpoint(sessionService));
        System.out.println(url + "/SessionEndpoint?wsdl");
        Endpoint.publish(url + "/ProjectEndpoint", new ProjectEndpoint(projectService, projectTaskService, sessionService));
        System.out.println(url + "/ProjectEndpoint?wsdl");
        Endpoint.publish(url + "/TaskEndpoint", new TaskEndpoint(taskService, projectTaskService, sessionService));
        System.out.println(url + "/TaskEndpoint?wsdl");
        Endpoint.publish(url + "/AdminEndpoint", new AdminEndpoint(userService, sessionService, domainService));
        System.out.println(url + "/AdminEndpoint?wsdl");
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(PID_FILE_NAME), pid.getBytes());
        @NotNull final File file = new File(PID_FILE_NAME);
        file.deleteOnExit();
    }

    public void initRootUser() {
        try {
            userService.add("root", "toor", "root@domain", Role.ADMIN, "Root", "Root", "Root");
        } catch (@NotNull final AbstractException e) {
            logService.error(e);
        }
    }

    public void run(@NotNull final String... args) {
        initEndpoints();
        initPID();
        backup.init();
        if (userService.isEmpty()) {
            logService.info("No users loaded. Initializing default root user.");
            initRootUser();
        }
    }

}
