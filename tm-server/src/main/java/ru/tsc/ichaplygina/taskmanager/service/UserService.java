package ru.tsc.ichaplygina.taskmanager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.empty.EmailEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithEmailException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithLoginException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.model.User;
import ru.tsc.ichaplygina.taskmanager.repository.UserRepository;

import java.sql.Connection;
import java.util.Objects;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.HashUtil.salt;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    @NotNull
    protected IUserRepository getRepository(@NotNull Connection connection) {
        return new UserRepository(connection);
    }

    @Override
    @SneakyThrows
    public final void add(@NotNull final String login, @NotNull final String password, @NotNull final String email, @NotNull final Role role,
                          @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            if (userRepository.isFoundByLogin(login)) throw new UserExistsWithLoginException(login);
            if (userRepository.isFoundByEmail(email)) throw new UserExistsWithEmailException(email);
            userRepository.add(new User(login, Objects.requireNonNull(salt(password, propertyService)), email, firstName, middleName, lastName, role));
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public final User findByLogin(@NotNull final String login) {
        @NotNull final String id = Optional.ofNullable(findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
        return findById(id);
    }

    @Nullable
    @Override
    public final User findByLoginForAuthorization(@NotNull final String login) {
        @NotNull final String id = Optional.ofNullable(findIdByLogin(login)).orElseThrow(IncorrectCredentialsException::new);
        return findById(id);
    }

    @Nullable
    @SneakyThrows
    private String findIdByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            return userRepository.findIdByLogin(login);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public final boolean isPrivilegedUser(@NotNull final String userId) {
        @NotNull final User user = Optional.ofNullable(findById(userId)).orElseThrow(UserNotFoundException::new);
        return user.getRole().equals(Role.ADMIN);
    }

    @Override
    @SneakyThrows
    public final boolean lockById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
            boolean result;
            result = userRepository.setLocked(id, true) != 0;
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean lockByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            @NotNull final String id = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new).getId();
            boolean result;
            result = userRepository.setLocked(id, true) != 0;
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public final User removeByLogin(@NotNull final String login) {
        @NotNull final String id = Optional.ofNullable(findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
        return removeById(id);
    }

    @Override
    @SneakyThrows
    public final void setPassword(@NotNull final String login, @NotNull final String password) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            @NotNull final String id = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new).getId();
            boolean result;
            userRepository.setPassword(id, salt(password, propertyService));
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public final void setRole(@NotNull final String login, @NotNull final Role role) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            @Nullable final String id = findIdByLogin(login);
            Optional.ofNullable(id).orElseThrow(UserNotFoundException::new);
            userRepository.setRole(id, role);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean unlockById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
            boolean result;
            result = userRepository.setLocked(id, false) != 0;
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean unlockByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            @NotNull final String id = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new).getId();
            boolean result;
            result = userRepository.setLocked(id, false) != 0;
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public final User updateById(@NotNull final String id, @NotNull final String login, @NotNull final String password, @NotNull final String email,
                                 @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            @Nullable final User userFoundWithThisLogin = userRepository.findByLogin(login);
            @Nullable final User userFoundWithThisEmail = userRepository.findByEmail(email);
            if (userFoundWithThisLogin != null && !id.equals(userFoundWithThisLogin.getId()))
                throw new UserExistsWithLoginException(login);
            if (userFoundWithThisEmail != null && !id.equals(userFoundWithThisEmail.getId()))
                throw new UserExistsWithEmailException(email);
            @NotNull final User user = Optional.ofNullable(userRepository.update(id, login, password, email, role, firstName, middleName, lastName))
                    .orElseThrow(UserNotFoundException::new);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }

    }

    @Nullable
    @Override
    @SneakyThrows
    public final User updateByLogin(@NotNull final String login, @NotNull final String password, @NotNull final String email,
                                    @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            @NotNull final String id = Optional.ofNullable(userRepository.findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
            @Nullable final User user = updateById(id, login, password, email, role, firstName, middleName, lastName);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
