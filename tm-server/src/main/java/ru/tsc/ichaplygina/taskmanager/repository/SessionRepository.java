package ru.tsc.ichaplygina.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.repository.ISessionRepository;
import ru.tsc.ichaplygina.taskmanager.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class SessionRepository extends AbstractModelRepository<Session> implements ISessionRepository {

    @NotNull
    private static final String TABLE_NAME = "TM_SESSION";

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    @SneakyThrows
    public void add(@NotNull Session session) {
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                "(ID, USER_ID, SIGNATURE, TIME_STAMP) VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getSignature());
        statement.setTimestamp(4, new Timestamp(session.getTimeStamp()));
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public @NotNull Session fetch(@NotNull ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString("ID"));
        session.setUserId(row.getString("USER_ID"));
        session.setSignature(row.getString("SIGNATURE"));
        session.setTimeStamp(row.getTimestamp("TIME_STAMP").getNanos());
        return session;
    }
}
