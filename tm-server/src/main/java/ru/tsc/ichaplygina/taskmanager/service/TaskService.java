package ru.tsc.ichaplygina.taskmanager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.repository.TaskRepository;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;

public final class TaskService extends AbstractBusinessEntityService<Task> implements ITaskService {

    @NotNull
    private final IUserService userService;

    public TaskService(@NotNull final IConnectionService connectionService, @NotNull final IUserService userService) {
        super(connectionService, userService);
        this.userService = userService;
    }

    @Override
    @NotNull
    protected ITaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public final Task addTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        try {
            @Nullable final Task task = userService.isPrivilegedUser(userId) ?
                    repository.addTaskToProject(taskId, projectId) : repository.addTaskToProjectForUser(userId, taskId, projectId);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public final List<Task> findAllByProjectId(@NotNull final String userId,
                                               @NotNull final String projectId,
                                               @Nullable final String sortBy) {
        @NotNull final Comparator<Task> comparator = getComparator(sortBy);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findAllByProjectId(projectId, comparator) : repository.findAllByProjectIdForUser(userId, projectId, comparator);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public final void removeAllByProjectId(@NotNull final String projectId) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        try {
            repository.removeAllByProjectId(projectId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public final Task removeTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        try {
            @Nullable final Task task = userService.isPrivilegedUser(userId) ?
                    repository.removeTaskFromProject(taskId, projectId) : repository.removeTaskFromProjectForUser(userId, taskId, projectId);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
