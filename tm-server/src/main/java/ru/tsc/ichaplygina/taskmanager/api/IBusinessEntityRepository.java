package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessEntityRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    void add(@NotNull String userId, @NotNull String name, @Nullable String description);

    void clearForUser(@NotNull String currentUserId);

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    @NotNull
    List<E> findAllForUser(@NotNull String userId);

    @NotNull
    List<E> findAllForUser(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    E findByIdForUser(@NotNull String userId, @NotNull String id);

    @Nullable
    E findByIndex(int index);

    @Nullable
    E findByIndexForUser(@NotNull String userId, int index);

    @Nullable
    E findByName(@NotNull String name);

    @Nullable
    E findByNameForUser(@NotNull String userId, @NotNull String name);

    @NotNull
    List<String> findKeysForUser(@NotNull String userId);

    @Nullable
    String getId(@NotNull String name);

    @Nullable
    String getId(int index);

    @Nullable
    String getIdForUser(@NotNull String userId, int index);

    @Nullable
    String getIdForUser(@NotNull String userId, String name);

    int getSizeForUser(@NotNull String userId);

    boolean isEmptyForUser(@NotNull String userId);

    boolean isNotFoundById(@NotNull String id);

    boolean isNotFoundByIdForUser(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeByIdForUser(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeByIndex(int index);

    @Nullable
    E removeByIndexForUser(@NotNull String userId, int index);

    @Nullable
    E removeByName(@NotNull String name);

    @Nullable
    E removeByNameForUser(@NotNull String userId, @NotNull String name);

    @Nullable
    E update(@NotNull String id, @NotNull String name, @Nullable String description);

    @Nullable
    E updateForUser(@NotNull String userId, @NotNull String id, @NotNull String name, @Nullable String description);

    @Nullable
    E updateStatusById(@NotNull String id, @NotNull Status status);

    @Nullable
    E updateStatusByIdForUser(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @Nullable
    E updateStatusByIndex(int index, @NotNull Status status);

    @Nullable
    E updateStatusByIndexForUser(@NotNull String userId, int index, @NotNull Status status);

    @Nullable
    E updateStatusByName(@NotNull String name, @NotNull Status status);

    @Nullable
    E updateStatusByNameForUser(@NotNull String userId, @NotNull String name, @NotNull Status status);

}
