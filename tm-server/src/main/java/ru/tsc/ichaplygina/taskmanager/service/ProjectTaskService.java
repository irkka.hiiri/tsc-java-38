package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public ProjectTaskService(@NotNull final ITaskService taskService, @NotNull final IProjectService projectService) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Nullable
    @Override
    public final Task addTaskToProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        if (isEmptyString(projectId) || isEmptyString(taskId)) throw new IdEmptyException();
        if (projectService.isNotFoundById(userId, projectId)) throw new ProjectNotFoundException();
        return (taskService.addTaskToProject(userId, taskId, projectId));
    }

    @Override
    public final void clearProjects(@NotNull final String userId) {
        projectService.findAll(userId).forEach(project -> taskService.removeAllByProjectId(project.getId()));
        projectService.clear(userId);
    }

    @NotNull
    @Override
    public final List<Task> findAllTasksByProjectId(@NotNull final String userId,
                                                    @NotNull final String projectId,
                                                    @NotNull final String sortBy) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        if (projectService.isNotFoundById(userId, projectId)) throw new ProjectNotFoundException();
        return taskService.findAllByProjectId(userId, projectId, sortBy);
    }

    @Nullable
    @Override
    public final Project removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        Optional.ofNullable(projectService.findById(userId, projectId)).orElseThrow(ProjectNotFoundException::new);
        taskService.removeAllByProjectId(projectId);
        return projectService.removeById(userId, projectId);
    }

    @Nullable
    @Override
    public final Project removeProjectByIndex(@NotNull final String userId, final int projectIndex) {
        if (isInvalidListIndex(projectIndex, projectService.getSize(userId)))
            throw new IndexIncorrectException(projectIndex + 1);
        return removeProjectById(userId, Optional.ofNullable(projectService.getId(userId, projectIndex)).orElseThrow(ProjectNotFoundException::new));
    }

    @Nullable
    @Override
    public final Project removeProjectByName(@NotNull final String userId, @NotNull final String projectName) {
        if (isEmptyString(projectName)) throw new NameEmptyException();
        return removeProjectById(userId, Optional.ofNullable(projectService.getId(userId, projectName)).orElseThrow(ProjectNotFoundException::new));
    }

    @Nullable
    @Override
    public final Task removeTaskFromProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        if (isEmptyString(projectId) || isEmptyString(taskId)) throw new IdEmptyException();
        if (projectService.isNotFoundById(userId, projectId)) throw new ProjectNotFoundException();
        return (taskService.removeTaskFromProject(userId, taskId, projectId));
    }

}
